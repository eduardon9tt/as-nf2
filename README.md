 = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = 
## AS-NF2

Automated script to create networkfilters with iptables.

### How to use

Start configuration:

    $ git clone https://gitlab.com/eduardon9tt/as-nf2.git && cd as-nf2
    $ sudo ./configure

Cleaning iptables (THIS WILL CLEAN ALL RULES, DELETE ALL CREATED CHAINS, ZERO COUNTERS AND TURN ALL DEFAULT POLICY TO ACCEPT):

    $ ./autoclean.sh

## Recomendation

When a connection is initiated, the 3 way handshake process comes into play to
ensure the connection. The client sends a packet with the marked SYN flag. The
server receives the packet and responds with a packet with the marked ACK/SYN 
flag. 

Then the client receives and sends another package with the marked ACK flag. 
Ready the connection was established between  the client and the server. To 
terminate  the connection  the client sends  a packet with  marked FIN flag. 

The server upon receiving the FIN flag package from the client responds with a 
packet with the flag also marked FIN.


Basic "rule" to identify safety irregularities:

There is no  one reason for  a certain  server  or  other machine to send 
packets  mainly  from internal networks  to  external networks  without a 
stimulus  or  connection  request. In a  classic  case  where  backdoor 
programs run  in the background and send packets without any stimulation.
Except  for some  implementations  where  packages are sent  without any 
stimulus.

Do not use these types of accept packets rules below:

    $ iptables -t filter -A INPUT -p protocol -j ACCEPT

Always declare state or status of the packages you will accept like this:

    $ iptables -t filter -A INPUT -p protocol -m conntrack --ctstate NEW -j ACCEPT
 
 or

    $ iptables -t filter -A INPUT -p protocol -m state --state NEW -j ACCEPT

#### Match conntrack --ctstate:

* NEW - The packet has started a new connection or otherwise associated with a connection which has not seen packets in both directions;
* NAT - A virtual state, matching if the original source address differs from the reply destination;
* DNAT - A virtual state, matching if the original destination differs from the reply source.
* INVALID - The packet is associated with no known connection;
* RELATED - The packet is starting a new connection, but is associated with an existing connection, such as an FTP data transfer or an ICMP error;
* UNTRACKED - The packet is not tracked at all, which happens if you explicitly untrack it by using -j CT --notrack in the raw table;
* ESTABLISHED - The packet is associated with a connection which has seen packets in both directions;

#### Match conntrack --ctstatus:

* NONE - None of the below;
* ASSURED - Conntrack entry should never be early-expired;
* EXPECTED - This is an expected connection (i.e. a conntrack helper set it up);
* CONFIRMED - Connection is confirmed: originating packet has left box;
* SEEN_REPLY - Conntrack has seen packets in both directions.


#### Table of TCP Flags:

* URG - The package contains important data;
* ACK - Certification that received the last package or other response;
* PSH - Send immediately even if the buffer is not full;
* RST - Resets the connection (error occurred or something like that);
* SYN - Initiates connection;
* FIN - Terminates connection.


### License:
MIT License
