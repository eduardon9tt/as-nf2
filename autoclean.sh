#!/bin/bash
# Project: AS-NF2
#
# Copyright 2019-now Eduardo Marques Santos
#
# AS-NF2 IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#
# See LICENSE file for usage of this software.
#
################################################################################
#
# AS-NF2 is a automated scripts for Debian GNU/Linux based systems.
#
################################################################################

WORK_DIR=`pwd`

INCLUDE_DIR=""
INCLUDE_FILES="./include"

ATA_DIR=""
DATA_FILES="./data"
for I in ${DATA_FILES}; do
  if [ "${I}" = "./data" ]; then
    if [ -d "${WORK_DIR}/data" ]; then
      DATA_DIR="${WORK_DIR}/data"
    fi
  elif [ -d ${I} -a -z "${DATA_DIR}" ]; then
      DATA_DIR="${I}"
  fi
done

if [ -z "${DATA_DIR}" ]; then
  printf "\n AS-NF2: Can't find data directory. Make sure to execute ${PROGRAM_NAME} from untarred directory or check the directories. \n"
  exit 1
fi

for I in ${INCLUDE_FILES}; do
  if [ "${I}" = "./include" ]; then
    if [ -d "${WORK_DIR}/include" ]; then
      INCLUDE_DIR="${WORK_DIR}/include"
    fi
  elif [ -d ${I} -a -z "${INCLUDE_DIR}" ]; then
      INCLUDE_DIR="${I}"
  fi
done

if [ -z "${INCLUDE_DIR}" ]; then
  printf "\n AS-NF2: Can't find include directory. Make sure to execute ${PROGRAM_NAME} from untarred directory or check the directories. \n"
  exit 1
fi

################################################################################

# Setting languages
if [ -f ${DATA_DIR}/languages/en ]; then
  . ${DATA_DIR}/languages/en
else
  printf "\n AS-NF2: Could not find languages directory.\n"
  exit 1
fi

if [ -x "$(command -v locale 2> /dev/null)" ]; then
  LANGUAGE=$(locale | egrep "^LANG=" | cut -d= -f2 | cut -d_ -f1 | egrep "^[a-z]{2}$")
fi

if [ -f ${DATA_DIR}/languages/${LANGUAGE} ]; then
  . ${DATA_DIR}/languages/${LANGUAGE}
fi

if [ -z "${LANGUAGE}" ]; then
  LANGUAGE="en"
fi

#################################################################################

if [ `id -u` -eq 0 ]; then
  PREVILEGE="0"
else
  PREVILEGE="1"
fi

#################################################################################

if [ -e "/usr/sbin/iptables" ]; then
  sleep .3s
  printf "\n ${FOUND_MSG0224}"
  NETFILTERv4="/usr/sbin/iptables"
  sleep .3s
elif [ -e "/sbin/iptables" ]; then
  sleep .3s
  printf "\n ${FOUND_MSG0224}"
  NETFILTERv4="/sbin/iptables"
  sleep .3s
else
  printf "\n ${ERROR_C01104B} \n"
  exit 1
fi

if [ -e "/usr/sbin/ip6tables" ]; then
  sleep .3s
  printf "\n ${FOUND_MSG0226}"
  NETFILTERv6="/usr/sbin/ip6tables"
  sleep .3s
elif [ -e "/sbin/ip6tables" ]; then
  sleep .3s
  printf "\n ${FOUND_MSG0226}"
  NETFILTERv6="/sbin/ip6tables"
  sleep .3s
else
  printf "\n ${ERROR_C01106B} \n"
  exit 1
fi

#################################################################################

printf "\n\n\n"
read -p "${ASK_A00C}" autoClean_ask
printf "\n"

if [ "${autoClean_ask}" == "y" ] || [ "${autoClean_ask}" == "Y" ]; then
		autoClean="1"
else 
		printf "\n Exit...\n\n"
		exit
fi

if [ -n "${autoClean}" ]; then
	if [ "${autoClean}" -eq 1 ]; then
		cleanRules="1"
	fi
fi


if [ -n "${cleanRules}" ]; then
	if [ "${cleanRules}" -eq 1 ]; then
		if [ -f ${INCLUDE_DIR}/nf_clean ]; then
			. ${INCLUDE_DIR}/nf_clean
		fi
	fi
fi