#!/bin/sh
# Project: AS-NF2
#
# Copyright 2019 Eduardo Marques Santos
#
# AS-NF2 IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#
# See LICENSE file for usage of this software.
#
################################################################################
#
# AS-NF2 is a automated scripts for Debian GNU/Linux based systems.
#
################################################################################




sleep .3s
printf "\n ${START_MSG0143}"
sleep .3s

###########################################################

${NETFILTERv4} -t filter -F
${NETFILTERv4} -t filter -Z
${NETFILTERv4} -t filter -X

###########################################################

# New Chains
${NETFILTERv4} -t filter -N networkfilters_rjt
${NETFILTERv4} -t filter -N networkfilters_allow
${NETFILTERv4} -t filter -N networkfilters_deny
${NETFILTERv4} -t filter -N networkfilters_unsec
${NETFILTERv4} -t filter -N networkfilters_in
${NETFILTERv4} -t filter -N networkfilters_fwd
${NETFILTERv4} -t filter -N networkfilters_out
  # Policy
${NETFILTERv4} -t filter -P INPUT ACCEPT
${NETFILTERv4} -t filter -P FORWARD ACCEPT
${NETFILTERv4} -t filter -P OUTPUT ACCEPT

###########################################################

# Jump INPUT to chain networkfilters_in
${NETFILTERv4} -t filter -A INPUT -j networkfilters_in
# Jump FORWARD to chain networkfilters_fwd
${NETFILTERv4} -t filter -A FORWARD -j networkfilters_fwd
# Jump OUTPUT to chain networkfilters_out
${NETFILTERv4} -t filter -A OUTPUT -j networkfilters_out

# Jump networkfilters_in to chain networkfilters_unsec
${NETFILTERv4} -t filter -A networkfilters_in -j networkfilters_unsec
# Jump networkfilters_fwd to chain networkfilters_unsec
${NETFILTERv4} -t filter -A networkfilters_fwd -j networkfilters_unsec
# Jump networkfilters_out to chain networkfilters_unsec
${NETFILTERv4} -t filter -A networkfilters_out -j networkfilters_unsec

###########################################################
## Rules for loopback interface

# Set rules to (lo) interface
${NETFILTERv4} -t filter -I INPUT 1 -i lo -p ALL -m conntrack ! --ctstatus NONE -j networkfilters_allow -m comment --comment " Loopback interface "
${NETFILTERv4} -t filter -I FORWARD 1 -i lo -p ALL -m conntrack ! --ctstatus NONE -j networkfilters_allow -m comment --comment " Loopback interface "
${NETFILTERv4} -t filter -I OUTPUT 1 -o lo -p ALL -m conntrack ! --ctstatus NONE -j networkfilters_allow -m comment --comment " Loopback interface "

###########################################################
## Main Rules

# Jump all input packets with state related,established to networkfilters_src
${NETFILTERv4} -t filter -A networkfilters_in -p tcp -m multiport --sports 0:65535 -m conntrack --ctstate RELATED,ESTABLISHED,UNTRACKED -j networkfilters_allow
${NETFILTERv4} -t filter -A networkfilters_in -p tcp -m multiport --dports 0:65535 -m conntrack --ctstate RELATED,ESTABLISHED,UNTRACKED -j networkfilters_allow
${NETFILTERv4} -t filter -A networkfilters_in -p udp -m multiport --sports 0:65535 -m conntrack --ctstate RELATED,ESTABLISHED,UNTRACKED -j networkfilters_allow
${NETFILTERv4} -t filter -A networkfilters_in -p udp -m multiport --dports 0:65535 -m conntrack --ctstate RELATED,ESTABLISHED,UNTRACKED -j networkfilters_allow

# Jump all forward packets with state related,established to networkfilters_src
${NETFILTERv4} -t filter -A networkfilters_fwd -p tcp -m multiport --sports 0:65535 -m conntrack --ctstate RELATED,ESTABLISHED,UNTRACKED -j networkfilters_allow
${NETFILTERv4} -t filter -A networkfilters_fwd -p tcp -m multiport --dports 0:65535 -m conntrack --ctstate RELATED,ESTABLISHED,UNTRACKED -j networkfilters_allow
${NETFILTERv4} -t filter -A networkfilters_fwd -p udp -m multiport --sports 0:65535 -m conntrack --ctstate RELATED,ESTABLISHED,UNTRACKED -j networkfilters_allow
${NETFILTERv4} -t filter -A networkfilters_fwd -p udp -m multiport --dports 0:65535 -m conntrack --ctstate RELATED,ESTABLISHED,UNTRACKED -j networkfilters_allow

# Jump all output packets with state new,related,established to networkfilters_dest
${NETFILTERv4} -t filter -A networkfilters_out -p tcp -m multiport --sports 0:65535 -m conntrack --ctstate NEW,RELATED,ESTABLISHED,UNTRACKED -j networkfilters_allow
${NETFILTERv4} -t filter -A networkfilters_out -p tcp -m multiport --dports 0:65535 -m conntrack --ctstate NEW,RELATED,ESTABLISHED,UNTRACKED -j networkfilters_allow
${NETFILTERv4} -t filter -A networkfilters_out -p udp -m multiport --sports 0:65535 -m conntrack --ctstate NEW,RELATED,ESTABLISHED,UNTRACKED -j networkfilters_allow
${NETFILTERv4} -t filter -A networkfilters_out -p udp -m multiport --dports 0:65535 -m conntrack --ctstate NEW,RELATED,ESTABLISHED,UNTRACKED -j networkfilters_allow

###########################################################

# Ping (it simply sends an ICMP type 8 (echo request) which all cooperative 
# hosts should obligingly respond to with an ICMP type 0 (echo reply) packet
${NETFILTERv4} -t filter -A networkfilters_in -p ICMP -m icmp --icmp-type 0 -m conntrack --ctstate RELATED,ESTABLISHED -j networkfilters_allow
${NETFILTERv4} -t filter -A networkfilters_out -p ICMP -m icmp --icmp-type 8 -m conntrack --ctstate NEW,RELATED,ESTABLISHED -j networkfilters_allow

${NETFILTERv4} -t filter -A networkfilters_in -p ICMP -m icmp --icmp-type 8 -m conntrack --ctstate NEW,RELATED,ESTABLISHED -j networkfilters_rjt
${NETFILTERv4} -t filter -A networkfilters_out -p ICMP -m icmp --icmp-type 0 -m conntrack --ctstate NEW,RELATED,ESTABLISHED -j networkfilters_rjt


###########################################################
## Insert on here rules to block unsecured packets.
## Dev's: After add conn limits of bytes, and hash limits.

# Block all traffic between the hours of 2AM and 3AM:
# ${NETFILTERv4} -t filter -A networkfilters_unsec -p TCP -m time --timestart 02:00 --timestop 03:00 -j DROP
# ${NETFILTERv4} -t filter -A networkfilters_unsec -p UDP -m time --timestart 02:00 --timestop 03:00 -j DROP
# ${NETFILTERv4} -t filter -A networkfilters_unsec -p ICMP -m time --timestart 02:00 --timestop 03:00 -j DROP

# Ping of death:
# ICMP type 8 (echo request)
${NETFILTERv4} -t filter -A networkfilters_unsec -p ICMP --icmp-type 8 -m limit --limit 1/s --limit-burst 5 -j networkfilters_allow

# Furtive port scanner:
${NETFILTERv4} -t filter -A networkfilters_unsec -p TCP --tcp-flags SYN,ACK,FIN,RST RST -m limit --limit 2/s --limit-burst 5 -j networkfilters_allow

# Syn-flood protection:
${NETFILTERv4} -t filter -A networkfilters_unsec -p TCP --syn -m limit --limit 30/s --limit-burst 4 -j networkfilters_allow

# Deny packets with INVALID state:
${NETFILTERv4} -t filter -A networkfilters_unsec -p TCP -m conntrack --ctstate INVALID -j networkfilters_deny

# Deny if the packets contain ALL flags active, XMAS packets.
#${NETFILTERv4} -t filter -A networkfilters_unsec -p TCP --tcp-flags ALL ALL -j networkfilters_deny

# Deny if the packets contain no flag active, Malformed packets.
#${NETFILTERv4} -t filter -A networkfilters_unsec -p TCP --tcp-flags ALL NONE -j networkfilters_deny

# Deny packets with NONE status:
${NETFILTERv4} -t filter -A networkfilters_unsec -p TCP -m conntrack --ctstatus NONE -j networkfilters_deny
${NETFILTERv4} -t filter -A networkfilters_unsec -p UDP -m conntrack --ctstatus NONE -j networkfilters_deny

###########################################################

${NETFILTERv4} -t filter -A networkfilters_unsec -p TCP -m recent --update --rsource --seconds 60 --name unsec-tcp -j networkfilters_rjt
${NETFILTERv4} -t filter -A networkfilters_unsec -p UDP -m recent --update --rsource --seconds 60 --name unsec-udp -j networkfilters_rjt
${NETFILTERv4} -t filter -A networkfilters_unsec -p ICMP -m recent --update --rsource --seconds 60 --name unsec-icmp -j networkfilters_rjt
${NETFILTERv4} -t filter -A networkfilters_rjt -p tcp -j REJECT --reject-with tcp-reset
${NETFILTERv4} -t filter -A networkfilters_rjt -p udp -j REJECT --reject-with icmp-port-unreachable
${NETFILTERv4} -t filter -A networkfilters_rjt -p icmp -j REJECT --reject-with icmp-host-prohibited
${NETFILTERv4} -t filter -A networkfilters_deny -j DROP
${NETFILTERv4} -t filter -A networkfilters_in -j DROP
${NETFILTERv4} -t filter -A networkfilters_fwd -j DROP
${NETFILTERv4} -t filter -A networkfilters_out -j DROP
${NETFILTERv4} -t filter -A networkfilters_allow -j ACCEPT

###########################################################
sleep .3s
printf "\n ${END_MSG0143} \n"
sleep .3s
TABLEFILTERCOMPLETE="1"
